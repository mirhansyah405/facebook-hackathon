package edu.mines.armap.helpers

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.Paint
import android.util.AttributeSet

import com.alexvasilkov.gestures.views.GestureImageView
import edu.mines.armap.canvasHight
import edu.mines.armap.canvasWidth

//import com.github.chrisbanes.photoview.PhotoView

class LinePhotoView : GestureImageView {
    constructor(c: Context): super(c)
    constructor(c: Context, a: AttributeSet): super(c, a)

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        val paint = Paint()
        paint.setColor(Color.RED)
        paint.setStrokeWidth(10f)

        canvasWidth = width
        canvasHight = height

        canvas?.drawLine(10.0f, height/2.0f, width.toFloat() - 10, height/2.0f, paint)
    }

//    fun getMatrix(): Matrix {
//        return this.imageMatrix
//    }

}