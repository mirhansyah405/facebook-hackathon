.. image:: ./logo-full.png

HypAR Map
=========

HypAR Map helps users navigate around unfamiliar locations by overlaying a map
of the area using augmented reality. The map can come from any photo (for
example, a fire escape plan) and is calibrated using an intuitive user
interface. HypAR Map then uses simultaneous location and mapping to find and
display the location of the user in the map in real-time.

Awards
------

- First Place at the 2018 Facebook Global Hackathon Finals

Video of our demo:

.. raw:: html

    <iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fhackathon%2Fvideos%2F2306855209387580%2F&width=500&show_text=false&height=281&appId" width="500" height="281" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media" allowFullScreen="true"></iframe>
